### Уважаемый Анатолий! 

Я не могу с вами связаться, поэтому сделал форк, чтоб вам пришло уведомление.

Я вижу, что вы пытались сделать ссылку на mytetra.xml, но у вас ничего не получилось - все время появлялись ссылки на конкретные состояния mytetra.xml, а не на самый актуальный файл mytetra.xml. 

Правильная ссылка на самый актуальный файл будет:

https://bitbucket.org/anatolean/mytetra/raw/master/mytetra.xml

Соответственно, ссылка на вашу базу в MyTetra Share будет:

http://webhamster.ru/mytetrashare/index/https:~~bitbucket.org~anatolean~mytetra~raw~master~mytetra.xml